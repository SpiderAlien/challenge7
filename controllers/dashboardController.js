const users = []
const {createUser, getAllUser,deleteUser} = require('../services/user')

module.exports = {
    dashboard: async(req,res,) => {
        const users = await getAllUser()
        console.log()
        res.render('dashboard',{
            users
        })
    },
    userAdd: (req, res) =>{
        res.render('/userAdd')
    },
    userAddpages: async (req, res) =>{

        const user = await createUser(req.body)
        users.push(user)
        // console.log(users)
        res.redirect('/dashboard')
    
    },
    delete: async(req,res) => {
        const userId = req.params.userId 
        await deleteUser(userId)
        const users = await getAllUser()
      
        res.render('dashboard',{
            users
        })
    }
}