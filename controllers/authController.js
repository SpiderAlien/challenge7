const {user_game} = require('../models')
const {createUser, userLogin} = require('../services/user')
const users = []

function format(user){
    const {id,email} = user
    return {
        id,
        email,
        accessToken :  user_game.generateToken()
    }
}

module.exports = {
    login: (req, res) =>{
        res.render("login")
    },
    loginPage: async (req,res) => {
    
        const user = await userLogin(req.body)   
    
        if(!user){
            return res.render('error')
        }
        const token = user_game.generateToken()
        res.cookie('token', token, {httpOnly: true, signed: true})
        console.log(token)
        res.redirect('/home')    
    },
    register: (req, res) =>{
        res.render('register')
    },
    registerPage: async (req, res) =>{
        console.log(req.body)
        const user = await createUser(req.body)
        users.push(user)
        // console.log(users)
        res.redirect('/auth/login')
    }
}