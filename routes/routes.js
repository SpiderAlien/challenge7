const { req } = require("express")
const express = require ("express")
const { game } = require("../controllers/routesController")
const routesController = require("../controllers/routesController")
const router = express.Router ()
const {authenticateUser} = require('../middleware/auth-jwt') 
const users = []

router.use((req, res, next) => {
    console.log("route level middleware")
    next()
})

router.get("/home", routesController.home)

router.get("/game", authenticateUser,routesController.game )

router.get("/userAdd", routesController.userAdd )

module.exports = router