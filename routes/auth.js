const { req, res } = require("express")
const express = require ("express")
const authController = require("../controllers/authController")
const router = express.Router ()
const users = []
const {user_game,user_game_biodata,user_game_histories} = require('../models')
const {createUser, userLogin} = require('../services/user')

router.use((req, res, next) => {
    console.log("route level middleware")
    next()
})

router.get("/login", authController.login)
router.post('/login',authController.loginPage )

router.get('/register', authController.register)
router.post('/register', authController.registerPage)


module.exports = router