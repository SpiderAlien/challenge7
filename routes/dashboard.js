const { response } = require('express')
const express = require('express')
const { DATE } = require('sequelize')
const dashboardController = require('../controllers/dashboardController')
const router = express.Router()
const users = []
const {authenticateUser} = require('../middleware/auth-jwt') 
const {user_game,user_game_biodata,user_game_histories} = require('../models')
const {createUser, getAllUser} = require('../services/user')

router.use((req,res,next)=>{
    console.log('router level middleware')
    next()
})

// router.get('/', (req,res,) => {
//     res.render('dashboard')
// })


router.get('/', authenticateUser, dashboardController.dashboard)

router.get('/userAdd', dashboardController.userAdd )
router.post('/userAdd', dashboardController.userAddpages)

router.delete('/dashboard/:userId', dashboardController.delete)

module.exports = router
