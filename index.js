const { request, response } = require("express")
const express = require("express")
const app = express()
const port = 3000
var cookieParser = require('cookie-parser');
var methodOverride = require('method-override')

app.use(express.urlencoded({extended: false}))
app.set('view engine', 'ejs')

app.use(express.static('public'))
app.use((request, response, next) => {
    next ()
})

app.use(cookieParser('ini rahasia'));

const homeRouter = require("./routes/routes.js")
app.use("/",homeRouter)

const authRouter = require("./routes/auth.js")
app.use("/auth",authRouter)

const dashboardRouter = require('./routes/dashboard')
app.use('/dashboard', dashboardRouter)
app.use(methodOverride('_method'))

// Internal Server error
app.use((req, res, next) => {
    res.status(500).json({
        status: 'fail',
        errors: err.message
    })
})
app.use ((err, req , res, next) => {
    res.status(404).json({
        status: 'fail',
        errors: 'yah salah'
    })
})

app.listen(port, () => {
    console.log(`web started at port : ${port}`)
})